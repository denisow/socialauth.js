


var sAuth = new function() {


//      ,------.                   ,--.                    ,-.,--.          ,--.                       ,--.                          ,---.    ,--.             ,--.    ,--.                       ,-.      
//      |  .--. ' ,---. ,--,--,  ,-|  | ,---. ,--.--.     / .'|  |-.  ,---. |  ,---.  ,--,--.,--.  ,--.`--' ,---. ,--.--.     ,---. /  .-'    |  |-. ,--.,--.,-'  '-.,-'  '-. ,---. ,--,--,  ,---.'. \     
//      |  '--'.'| .-. :|      \' .-. || .-. :|  .--'    |  | | .-. '| .-. :|  .-.  |' ,-.  | \  `'  / ,--.| .-. ||  .--'    | .-. ||  `-,    | .-. '|  ||  |'-.  .-''-.  .-'| .-. ||      \(  .-' |  |    
//      |  |\  \ \   --.|  ||  |\ `-' |\   --.|  |       |  | | `-' |\   --.|  | |  |\ '-'  |  \    /  |  |' '-' '|  |       ' '-' '|  .-'    | `-' |'  ''  '  |  |    |  |  ' '-' '|  ||  |.-'  `)|  |    
//      `--' '--' `----'`--''--' `---'  `----'`--'        \ '. `---'  `----'`--' `--' `--`--'   `--'   `--' `---' `--'        `---' `--'       `---'  `----'   `--'    `--'   `---' `--''--'`----'.' /     
//                                                         `-'                                                                                                                                    `-'      


    this.disableAuthButtons = function(){
        $(".socialAuthBtn").prop("disabled", true);
    }

    this.setsocialSignOutBtnAbility = function(){
        $("#socialSignOutBtn").prop("disabled", !( $("#googleSignInBtn").prop("disabled") || $("#FBSignInBtn").prop("disabled") || $("#VKSignInBtn").prop("disabled")) );
    }

    this.googleLoginRender = function(logIn){
        $("#googleSignInBtn").prop("disabled", logIn);
        $("#googleSignOutBtn").prop("disabled", !logIn);        
        sAuth.setsocialSignOutBtnAbility();
    }
    this.fbLoginRender = function(logIn){
        $("#FBSignInBtn").prop("disabled", logIn);
        $("#FBSignOutBtn").prop("disabled", !logIn);
        sAuth.setsocialSignOutBtnAbility();

    }
    this.vkLoginRender = function(logIn){
        $("#VKSignInBtn").prop("disabled", logIn);
        $("#VKSignOutBtn").prop("disabled", !logIn);
        sAuth.setsocialSignOutBtnAbility();        
    }



//      ,--.        ,--.  ,--.  ,--.        ,--.,--.                 ,--.  ,--.                
//      |  |,--,--, `--',-'  '-.`--' ,--,--.|  |`--',-----. ,--,--.,-'  '-.`--' ,---. ,--,--,  
//      |  ||      \,--.'-.  .-',--.' ,-.  ||  |,--.`-.  / ' ,-.  |'-.  .-',--.| .-. ||      \ 
//      |  ||  ||  ||  |  |  |  |  |\ '-'  ||  ||  | /  `-.\ '-'  |  |  |  |  |' '-' '|  ||  | 
//      `--'`--''--'`--'  `--'  `--' `--`--'`--'`--'`-----' `--`--'  `--'  `--' `---' `--''--' 
//

    this.userRenderCallback = function(user){};
    this.user = {idGoogle:"", idFB:"", idVK:"", email: "" ,name:"", city:"", avaBig:"", avaSmall:""};


//  SESSION

    this.sessionUser = function(callback){
        var thereareUser = false;
        sessionUser = $.parseJSON(sessionStorage.getItem("sAuthUser"));
        if(sessionUser!=null){
            thereareUser = true;
            sAuth.user = sessionUser
        }
        callback(sAuth.user);
        return thereareUser;
    }
    
    this.saveUserSession = function(){
        sessionStorage.setItem("sAuthUser", JSON.stringify(sAuth.user));
    }

sessionStorage


//   GOOGLE

    this.initGoogleAuth = function(clientId, callback){
        this.userRenderCallback = callback;
        var additionalParams = { 
          "callback": function(authResult){
            if(authResult["access_token"]) {
                gapi.client.load("plus","v1", sAuth.getGoogleUser);
                sAuth.googleLoginRender(true);
            } else {
                sAuth.googleLoginRender(false);
            }        
          },
          "clientid": clientId, 
          "cookiepolicy": "single_host_origin",             
          "requestvisibleactions": "http://schemas.google.com/AddActivity",
          "scope": "https://www.googleapis.com/auth/plus.login"
        };
        $("#googleSignInBtn").prop("disabled", false);
        gapi.signin.render("googleSignInBtn", additionalParams);
        sAuth.disableAuthButtons();


        $("#googleSignOutBtn, #socialSignOutBtn").click(function(){
            if(sAuth.user.idGoogle == ""){
                return;
            }            
            $.ajax({
              type: "GET",
              url: "https://accounts.google.com/o/oauth2/revoke?token=" + gapi.auth.getToken().access_token,
              async: false,
              contentType: "application/json",
              dataType: "jsonp",
              success: function(result) {
                sAuth.googleLoginRender(false);
                sAuth.user.idGoogle = "";
                sAuth.cleanUserInfo();
              }
            });
        });
    }


//   FACEBOOK

    this.initFBAuth = function(fbAppId, callback) {
        this.userRenderCallback = callback;
        FB.init({
            appId: fbAppId, status: true, cookie: true, xfbml: true 
        });        


        sAuth.disableAuthButtons();
        

        $("#FBSignInBtn").click(function(){
          FB.login(function(response) {
            if (response.authResponse) {
                sAuth.fbLoginRender(true);
                sAuth.getFBUser();
            } else {
                sAuth.fbLoginRender(false);
            }
          });
        });
        $("#FBSignOutBtn, #socialSignOutBtn").click(function(){
            if(sAuth.user.idFB == ""){
                return;
            }            
            FB.api("/me/permissions", "delete", function(response){ 
                sAuth.user.idFB = "";
                sAuth.cleanUserInfo();
                sAuth.fbLoginRender(false);
            });
        });
        FB.getLoginStatus(function(response) {
            if (response.status === "connected") {
                sAuth.getFBUser();
                sAuth.fbLoginRender(true);
            } else {
                sAuth.fbLoginRender(false);
            }
        });
    }



//   VKONTAKTE

    this.initVKAuth = function(vkAppId, callback) {
        this.userRenderCallback = callback;
        VK.init({apiId: vkAppId});

        sAuth.disableAuthButtons();

        VK.Auth.getLoginStatus(function(response) {
            if (response.session) {
                sAuth.vkLoginRender(true);
                sAuth.getVKUser();
            } else {        
                sAuth.vkLoginRender(false);
            }
        });
        $("#VKSignInBtn").click(function(){
            VK.Auth.login(function(response) {
                if (response.session) {
                    sAuth.vkLoginRender(true);
                    sAuth.getVKUser();
                } else {
                    sAuth.vkLoginRender(false);
                }
            });
          });

        $("#VKSignOutBtn, #socialSignOutBtn").click(function(){
            if(sAuth.user.idVK == ""){
                return;
            }            
            VK.Auth.logout(function(response) {
                sAuth.user.idVK = "";
                sAuth.cleanUserInfo();
                sAuth.vkLoginRender(false);
            });
        });

    }



                                                                                                                                                                     
//      ,------.                       ,--.                                                    ,--.         ,---.                                  ,--.  ,--.                
//      |  .--. ' ,--,--.,--.--. ,---. `--',--,--,  ,---.     ,--.,--. ,---.  ,---. ,--.--.    `--',--,--, /  .-' ,---. ,--.--.,--,--,--. ,--,--.,-'  '-.`--' ,---. ,--,--,  
//      |  '--' |' ,-.  ||  .--'(  .-' ,--.|      \| .-. |    |  ||  |(  .-' | .-. :|  .--'    ,--.|      \|  `-,| .-. ||  .--'|        |' ,-.  |'-.  .-',--.| .-. ||      \ 
//      |  | --' \ '-'  ||  |   .-'  `)|  ||  ||  |' '-' '    '  ''  '.-'  `)\   --.|  |       |  ||  ||  ||  .-'' '-' '|  |   |  |  |  |\ '-'  |  |  |  |  |' '-' '|  ||  | 
//      `--'      `--`--'`--'   `----' `--'`--''--'.`-  /      `----' `----'  `----'`--'       `--'`--''--'`--'   `---' `--'   `--`--`--' `--`--'  `--'  `--' `---' `--''--' 
//                                                 `---'                                                                                                                     


    this.getGoogleUser = function(){
        var request = gapi.client.plus.people.get( {"userId" : "me"} );
        request.execute( function(inf) {
            sAuth.user.idGoogle = inf.id;
            sAuth.user.name = inf.displayName;
            sAuth.user.city = inf.placesLived[0].value;
            sAuth.user.avaBig = inf.image.url.replace("?sz=50","?sz=100");
            sAuth.user.avaSmall = inf.url.image;

            sAuth.saveUserSession();
            sAuth.userRenderCallback(sAuth.user);
        });      
    }



    this.getFBUser = function(){
        FB.api("/me/?locale=ru_RU&fields=id,name,location,picture", function(inf) {               
            sAuth.user.idFB = inf.id;
            sAuth.user.name = inf.name;
            sAuth.user.city = inf.location.name;
            sAuth.user.avaBig = "http://graph.facebook.com/"+inf.id+"/picture?height=100&width=100";
            sAuth.user.avaSmall = inf.picture.data.url;

            sAuth.saveUserSession();
            sAuth.userRenderCallback(sAuth.user);
        });
    }  



    this.getVKUser = function(){
        VK.Api.call("users.get", {fields:"photo_100,photo_50,city,screen_name", v:"5.20"} , function(r) {
            if(r.response) {
                var inf = r.response[0]
                sAuth.user.idVK = inf.id;
                sAuth.user.name = inf.first_name+" "+inf.last_name;
                sAuth.user.city = inf.city.title;
                sAuth.user.avaBig = inf.photo_100,
                sAuth.user.avaSmall =inf.photo_50;

                sAuth.saveUserSession();
                sAuth.userRenderCallback(sAuth.user);
            }
        });
    }
    this.cleanUserInfo = function(){
        sAuth.user.email = "";
        sAuth.user.name = "";
        sAuth.user.city = "";
        sAuth.user.avaBig = "",
        sAuth.user.avaSmall = "";
        sAuth.userRenderCallback(sAuth.user);
    }
}
